<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller {
    public function redirect() {
        return Socialite::driver('google')->redirect();
    }

    public function callback() {
        $oauthUserData = Socialite::driver('google')->user();

        $user = User::updateOrCreate(
            [ 'oauth_id' => $oauthUserData->id ],
            [
                'oauth_image_url' => $oauthUserData->avatar,
                'first_name' => $oauthUserData->user['given_name'],
                'last_name' => $oauthUserData->user['family_name'],
                'email' => $oauthUserData->user['email']
            ]
        );

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
