<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class TokenProviderController extends Controller {
    public function provide(Request $request) {
        $email = $request->email;
        $tokenName = $request->token_name;

        $user = User::where('email', $email)->first();

        $token = $user->createToken($tokenName);

        return [ 'token' => $token->plainTextToken ];
    }
}
