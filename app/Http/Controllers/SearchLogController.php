<?php

namespace App\Http\Controllers;

use App\Models\SearchLog;
use Illuminate\Http\Request;

class SearchLogController extends Controller {
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SearchLog  $searchLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SearchLog $searchLog) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SearchLog  $searchLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(SearchLog $searchLog) {
        //
    }
}
