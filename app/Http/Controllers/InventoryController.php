<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Inventory;
use App\Models\SearchLog;
use App\Models\Subcat;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

use FastExcel;

class InventoryController extends Controller {

    public function filters(Request $request) {
        $conditions = collect($request->conditions);
        $categories = collect($request->categories);

        $inventoryCategories = Inventory::where('arc', 'False')
            ->where(function($query) use ($categories) {
                if (!$categories->contains('Zero QTY')) {
                    $query->where('qty', '>', 0);
                }
                if (!$categories->contains('Blank')) {
                    $query->where('con', '!=', '');
                }
            })
            ->orderBy('cat')
            ->distinct()
            ->get('cat')
            ->map(fn($category) => $category['cat']);

        $inventoryCondition = Inventory::where('arc', 'False')
            ->where(function($query) use ($conditions) {
                if (!$conditions->contains('Zero QTY')) {
                    $query->where('qty', '>', 0);
                }

                if (!$conditions->contains('Blank')) {
                    $query->where('con', '!=', '');
                }
            })
            ->orderBy('con')
            ->distinct()
            ->get('con')
            ->map(fn($condition) => $condition['con']);

        $allFilters = Category::select('categories.cat_name', 'subcats.category_name', 'subcats.display_name')
            ->leftJoin('subcats', 'categories.cat_id', '=', 'subcats.cat_id')
            ->whereNotNull('subcats.category_name')
            ->orderBy('categories.cat_order')
            ->orderBy('subcats.subcat_order')
            ->get();

        $allFilters = $allFilters->filter(
            fn($filter) => $inventoryCondition->map(fn($filter) => strtolower($filter))
                ->contains(strtolower($filter['category_name'])) || $filter['category_name'] === 'Blank' ||
            $filter['category_name'] === 'Zero QTY' ||
            $inventoryCategories->map(fn($filter) => strtolower($filter))
                ->contains(strtolower($filter['category_name']))
        )->values();

        $filters = [];

        foreach($allFilters as $filter) {
            $filters[$filter['cat_name']][] = [
                'name' => $filter['display_name'],
                'fullName' => $filter['category_name']
            ];
        }

        return $filters;
    }

    public function search(Request $request) {
        $searchTerm = $request->post('searchTerm');
        $categories = collect($request->post('categories'));
        $paramsConditions = collect($request->post('conditions'));
        $sortedBy = $request->post('sortedBy');
        $sortedAscending = $request->boolean('sortedAscending');

        $qtyZeroShown = $paramsConditions->contains('Zero QTY');
        $blankShown = $paramsConditions->contains('Blank');
        $conditions = $paramsConditions->filter(fn($filter, $key) => $filter !== 'Zero QTY' && $filter  !== 'Blank');
        $hasCategories = $categories->count() > 0;
        $hasConditions = $conditions->count() > 0;

        $searchSegments = explode(' ', $searchTerm);
        $sortingCustomized = strlen($sortedBy) > 0;

        if ($sortingCustomized) {
            $sortingColumn = $sortedBy;
        } else {
            $sortingColumn = 'par';
        }

        $summaryResult = Inventory::select(Inventory::raw('COUNT(*) AS summaryCount'))
            ->where('arc', 'False')
            ->when(!$qtyZeroShown, fn($query) => $query->where('qty', '>', 0))
            ->where(function($query) use ($searchSegments) {
                foreach ($searchSegments as $segment) {
                    $query->where(fn($group) => $group->where('par', 'LIKE', "%{$segment}%")
                        ->orWhere('bar' , 'LIKE', "%{$segment}%")
                        ->orWhere('des' , 'LIKE', "%{$segment}%"));
                }
            })
            ->when($hasCategories, function($query) use ($categories) {
                $query->whereIn('cat', $categories);
            })
            ->when($hasConditions, function($query) use ($conditions) {
                $mappedFilters = $conditions->map(fn($filter, $key) => $filter === 'Blank' ? '' : $filter);
                $query->whereIn('con' , $mappedFilters);
            })
            ->first();

        $result = Inventory::select(
            'par',
            'bar',
            'des',
            'con',
            'cat',
            'pri',
            'qty',
            Inventory::raw('ROUND(pri * qty, 2) AS total'),
            'bin'
        )
            ->where('arc', 'False')
            ->when(!$qtyZeroShown, fn($query) => $query->where('qty', '>', 0))
            ->where(function($query) use ($searchSegments) {
                foreach($searchSegments as $segment) {
                    $query->where(fn($group) => $group->where('par', 'LIKE', "%{$segment}%")
                        ->orWhere('bar' , 'LIKE', "%{$segment}%")
                        ->orWhere('des' , 'LIKE', "%{$segment}%"));
                }
            })
            ->when($hasCategories, function($query) use ($categories) {
                $query->whereIn('cat', $categories);
            })
            ->when($hasConditions, function($query) use ($conditions) {
                $mappedFilters = $conditions->map(fn($filter, $key) => $filter === 'Blank' ? '' : $filter);
                $query->whereIn('con' , $mappedFilters);
            })
            ->orderBy($sortingColumn, $sortedAscending ? 'ASC' : 'DESC')
            ->paginate(100);

        $response = collect([
            'summaryCount' => $summaryResult->summaryCount,
            'items' => $result
        ]);

        return $response;
    }

    public function logQuery(Request $request) {
        $query = $request->search_query;
        $user = $request->user();

        SearchLog::insert([
            'query' => $query,
            'user_id' => $user->id
        ]);
    }

    public function exportCsv(Request $request) {
        $paramSearchTerm = $request->get('query');
        $paramFilters = $request->get('filters');
        $paramCategories = $request->get('categories');
        $paramConditions = $request->get('conditions');

        $searchTerm = isset($paramSearchTerm) ? $paramSearchTerm : '';
        $categories = isset($paramCategories) ? collect(explode(',', $paramCategories)) : null;
        $conditions = isset($paramConditions) ? collect(explode(',', $paramConditions)) : null;
        $sortedBy = $request->get('sortedBy');
        $sortedAscending = $request->get('sortedAscending');

        $qtyZeroShown = isset($paramConditions) && $paramConditions->contains('Zero QTY');

        if ($qtyZeroShown) {
            $conditions = $conditions->filter(fn($condition, $key) => $condition !== 'Zero QTY');
        }

        $hasCategories = isset($categories) && $categories->count() > 0;
        $hasConditions = isset($conditions) && $conditions->count() > 0;

        $result = Inventory::select(
            'par',
            'bar',
            'des',
            'con',
            'cat',
            'pri',
            'qty',
            Inventory::raw('ROUND(pri * qty, 2) AS total'),
            'bin'
        )
            ->where('arc', 'False')
            ->when(!$qtyZeroShown, fn($query) => $query->where('qty', '>', 0))
            ->where(function($query) use ($searchTerm) {
                $query->where('par', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('bar' , 'LIKE', "%{$searchTerm}%")
                    ->orWhere('des' , 'LIKE', "%{$searchTerm}%");
            })
            ->when($hasCategories, function($query) use ($categories) {
                $query->whereIn('cat', $categories);
            })
            ->when($hasConditions, function($query) use ($conditions) {
                $mappedFilters = $conditions->map(fn($filter, $key) => $filter === 'Blank' ? '' : $filter);
                $query->whereIn('con' , $mappedFilters);
            })
            ->orderBy($sortedBy, $sortedAscending ? 'ASC' : 'DESC')
            ->get();

        $date = date_create('now')->format('Y-m-d-H-i-s');

        return FastExcel::data($result)->download("inventory-{$date}.csv");
    }
}
