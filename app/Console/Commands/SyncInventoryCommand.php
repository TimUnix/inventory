<?php

namespace App\Console\Commands;

use App\Models\Inventory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use Exception;
use FastExcel;

class SyncInventoryCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inventory:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize inventory data with linnworks export.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {

        $this->info('Truncating inventory..');
        Inventory::truncate();

        $path = env('CSV_DUMP_PATH');
        $file = $path."unixsurplus.csv";

        $this->info('Synchronizing inventory data...');

        DB::connection()->getpdo()->exec('
            LOAD DATA LOCAL INFILE "' . $file . '"
            INTO TABLE inventory_items
            FIELDS
                TERMINATED by \',\'
                OPTIONALLY ENCLOSED BY \'"\'
                ESCAPED BY \'\'
            LINES
                TERMINATED BY \'\r\n\'
                (par, bar, man, con, pri, qty, des, cat, bin, arc, loc)
        ');

        $this->info('Completed inventory data synchronization.');

        return 0;
    }
}
