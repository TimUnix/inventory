<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;

    protected $table = 'inventory_items';
    protected $primaryKey = 'par';
    protected $keyType = 'string';
    protected $fillable = [
        'par',
        'bar',
        'man',
        'con',
        'pri',
        'qty',
        'des',
        'cat',
        'bin',
        'arc',
        'loc'
    ];
    public $incrementing = false;
    public $timestamps = false;
}
