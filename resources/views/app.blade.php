<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title inertia>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        @vite('resources/js/app.js')
        <!-- Scripts -->
        @routes
		<script type="text/javascript" async defer src="https://cdn.reamaze.com/assets/reamaze.js"></script>
		<script type="text/javascript"> var _support = _support || { 'ui': {}, 'user': {} }; _support['account'] = 'requestforquote'; _support['ui']['contactMode'] = 'default'; _support['ui']['enableKb'] = 'false'; _support['ui']['styles'] = { widgetColor: 'rgb(34, 48, 64)', }; _support['ui']['widget'] = { icon: 'chat', displayOn: 'all', label: { text: 'Let us know if you have any questions! &#128522;', mode: "notification", delay: 3, duration: 30, sound: true, }, position: 'bottom-right', mobilePosition: 'bottom-right' }; _support['apps'] = { faq: {"enabled":false}, recentConversations: {}, orders: {"enabled":false,"header":"Big Commerce Order Status"} }; </script>
		<script async defer src="https://www.googletagmanager.com/gtag/js?id=G-N23B10TS9N"></script>
        @inertiaHead
    </head>
    <body class="font-sans antialiased">
        @inertia
    </body>
</html>
