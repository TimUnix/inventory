import './bootstrap';
import '../css/app.css';

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/vue3';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faPlus,
    faTimes,
    faSearch,
    faCopy,
    faCheckDouble,
    faClipboard,
    faSortUp,
    faSortDown
} from '@fortawesome/free-solid-svg-icons';
import { faGoogle } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

library.add(faPlus, faTimes, faSearch, faCopy, faCheckDouble, faClipboard, faGoogle, faSortUp, faSortDown);

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    progress: { color: '#4B5563' },
    resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
    setup({ el, App, props, plugin }) {
        return createApp({ render: () => h(App, props) })
            .use(plugin)
			.component('FontAwesomeIcon', FontAwesomeIcon)
            .mixin({ methods: { route } })
            .mount(el);
    },
});

