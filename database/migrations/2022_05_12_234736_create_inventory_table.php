<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory', function(Blueprint $table) {
            $table->string('par', 255)->primary();
            $table->string('bar', 255);
            $table->string('man', 100);
            $table->string('con', 25);
            $table->decimal('pri');
            $table->integer('qty');
            $table->text('des');
            $table->string('cat', 50);
            $table->string('bin', 100);
            $table->string('arc', 25);
            $table->string('loc', 25);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory');
    }
}
