<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\Auth\TokenProviderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/inventory/search', [InventoryController::class, 'search']);
Route::post('/inventory/filters', [InventoryController::class, 'filters']);
Route::post('/tokens/create', [TokenProviderController::class, 'provide']);

Route::middleware('auth:sanctum')->post('/inventory/search/log', [InventoryController::class, 'logQuery']);
